package AerialVehicles.Planes;


import AerialVehicles.Capabilities.Assessor;
import AerialVehicles.Capabilities.Bomber;
import Entities.Coordinates;

public class F16 extends Plane implements Bomber, Assessor {
    private int missilesAmount;
    private Bomber.missileType missileType;
    private Assessor.cameraType cameraType;

    public F16(Coordinates homeBase,
               int flightHours,
               Status status,
               int missilesAmount,
               Bomber.missileType missileType,
               Assessor.cameraType cameraType) {
        super(homeBase, flightHours, status);
        this.setMissilesAmount(missilesAmount);
        this.setMissileType(missileType);
        this.setCameraType(cameraType);
    }

    public F16(Coordinates homeBase,
               int missilesAmount,
               Bomber.missileType missileType,
               Assessor.cameraType cameraType) {
        super(homeBase);
        this.setMissilesAmount(missilesAmount);
        this.setMissileType(missileType);
        this.setCameraType(cameraType);
    }

    public int missilesAmount() {
        return missilesAmount;
    }

    private void setMissilesAmount(int missilesAmount) {
        this.missilesAmount = missilesAmount;
    }

    public Bomber.missileType missileType() {
        return missileType;
    }

    private void setMissileType(Bomber.missileType missileType) {
        this.missileType = missileType;
    }

    public Assessor.cameraType cameraType() {
        return cameraType;
    }

    private void setCameraType(Assessor.cameraType cameraType) {
        this.cameraType = cameraType;
    }
}
