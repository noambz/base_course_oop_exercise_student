package AerialVehicles.Planes;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Plane extends AerialVehicle {
    private static final int maxFlightHourUntilRepair = 250;

    public Plane(Coordinates homeBase, int flightHours, Status status) {
        super(homeBase, flightHours, status);
    }

    public Plane(Coordinates homeBase) {
        super(homeBase);
    }

    public int maxFlightHourUntilRepair() {
        return maxFlightHourUntilRepair;
    }
}
