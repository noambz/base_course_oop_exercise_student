package AerialVehicles.Drones.Haron;


import AerialVehicles.Capabilities.Assessor;
import AerialVehicles.Capabilities.Bomber;
import AerialVehicles.Capabilities.Gather;
import Entities.Coordinates;

public class Shoval extends Haron implements Bomber, Gather, Assessor {
    private int missilesAmount;
    private Bomber.missileType missileType;
    private Gather.sensorType sensorType;
    private Assessor.cameraType cameraType;

    public Shoval(Coordinates homeBase,
                  int flightHours,
                  Status status,
                  int missilesAmount,
                  Bomber.missileType missileType,
                  Gather.sensorType sensorType,
                  Assessor.cameraType cameraType) {
        super(homeBase, flightHours, status);
        this.setMissilesAmount(missilesAmount);
        this.setMissileType(missileType);
        this.setSensorType(sensorType);
        this.setCameraType(cameraType);
    }

    public Shoval(Coordinates homeBase,
                  int missilesAmount,
                  Bomber.missileType missileType,
                  Gather.sensorType sensorType,
                  Assessor.cameraType cameraType) {
        super(homeBase);
        this.setMissilesAmount(missilesAmount);
        this.setMissileType(missileType);
        this.setSensorType(sensorType);
        this.setCameraType(cameraType);
    }

    public int missilesAmount() {
        return missilesAmount;
    }

    private void setMissilesAmount(int missilesAmount) {
        this.missilesAmount = missilesAmount;
    }

    public Bomber.missileType missileType() {
        return missileType;
    }

    private void setMissileType(Bomber.missileType missileType) {
        this.missileType = missileType;
    }

    public Gather.sensorType sensorType() {
        return sensorType;
    }

    private void setSensorType(Gather.sensorType sensorType) {
        this.sensorType = sensorType;
    }

    public Assessor.cameraType cameraType() {
        return cameraType;
    }

    private void setCameraType(Assessor.cameraType cameraType) {
        this.cameraType = cameraType;
    }
}

