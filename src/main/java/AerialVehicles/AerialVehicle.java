package AerialVehicles;

import Entities.Coordinates;

public abstract class AerialVehicle {
    public static enum Status {
        READY,
        NOT_READY,
        ACTIVE
    }

    private Coordinates homeBase;
    private int flightHours;
    private Status status;

    public AerialVehicle(Coordinates homeBase, int flightHours, Status status) {
        this.setHomeBase(homeBase);
        this.setFlightHours(flightHours);
        this.setStatus(status);
    }

    public AerialVehicle(Coordinates homeBase) {
        this.setHomeBase(homeBase);
        this.resetFlightHours();
        this.setStatus(Status.READY);
    }

    public Coordinates homeBase() {
        return homeBase;
    }

    private void setHomeBase(Coordinates homeBase) {
        this.homeBase = homeBase;
    }

    public Status status() {
        return status;
    }

    protected void setStatus(Status status) {
        this.status = status;
    }

    public int flightHours() {
        return flightHours;
    }

    private void setFlightHours(int flightHours) {
        this.flightHours = flightHours;
    }

    private void resetFlightHours() {
        this.setFlightHours(0);
    }

    public abstract int maxFlightHourUntilRepair();

    public void repair() {
        this.resetFlightHours();
        this.setStatus(Status.READY);
    }

    public void check() {
        if (this.flightHours < this.maxFlightHourUntilRepair()) {
            this.setStatus(Status.READY);
        } else {
            this.setStatus(Status.NOT_READY);
            this.repair();
        }
    }

    public void flyTo(Coordinates destination) {
        if (this.status() == Status.NOT_READY) {
            System.out.println("Aerial Vehicle is not ready to fly");
        } else {
            this.setStatus(Status.ACTIVE);
            System.out.printf("%s Flying to: %s\n", this.getClass().getSimpleName(), destination);
        }
    }

    public void land(Coordinates destination) {
        if (this.status() == Status.ACTIVE) {
            System.out.printf("%s Landing on: %s\n", this.getClass().getSimpleName(), destination);
            this.check();
        } else {
            System.out.println("Aerial Vehicle is not active");
        }
    }
}
