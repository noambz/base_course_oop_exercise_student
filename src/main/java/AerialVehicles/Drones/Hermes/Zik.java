package AerialVehicles.Drones.Hermes;

import AerialVehicles.Capabilities.Assessor;
import AerialVehicles.Capabilities.Gather;
import Entities.Coordinates;

public class Zik extends Hermes implements Gather, Assessor {
    private sensorType sensorType;
    private cameraType cameraType;

    public Zik(Coordinates homeBase,
               int flightHours,
               Status status,
               Gather.sensorType sensorType,
               Assessor.cameraType cameraType) {
        super(homeBase, flightHours, status);
        this.setSensorType(sensorType);
        this.setCameraType(cameraType);
    }

    public Zik(Coordinates homeBase,
               Gather.sensorType sensorType,
               Assessor.cameraType cameraType) {
        super(homeBase);
        this.setSensorType(sensorType);
        this.setCameraType(cameraType);
    }

    public Gather.sensorType sensorType() {
        return sensorType;
    }

    private void setSensorType(Gather.sensorType sensorType) {
        this.sensorType = sensorType;
    }

    public Assessor.cameraType cameraType() {
        return cameraType;
    }

    private void setCameraType(Assessor.cameraType cameraType) {
        this.cameraType = cameraType;
    }
}