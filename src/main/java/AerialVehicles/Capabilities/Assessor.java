package AerialVehicles.Capabilities;

public interface Assessor {
    public static enum cameraType {
        Regular,
        Thermal,
        NightVision
    }

    cameraType cameraType();
}
