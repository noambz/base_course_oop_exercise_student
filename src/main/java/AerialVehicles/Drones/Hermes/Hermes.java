package AerialVehicles.Drones.Hermes;

import AerialVehicles.Drones.Drone;
import Entities.Coordinates;

public abstract class Hermes extends Drone {
    private static final int maxFlightHourUntilRepair = 100;

    public Hermes(Coordinates homeBase, int flightHours, Status status) {
        super(homeBase, flightHours, status);
    }

    public Hermes(Coordinates homeBase) {
        super(homeBase);
    }

    public int maxFlightHourUntilRepair() {
        return maxFlightHourUntilRepair;
    }
}
