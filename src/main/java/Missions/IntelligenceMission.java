package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.Capabilities.Gather;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates destination,
                               String pilotName,
                               AerialVehicle aircraft,
                               String region) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, aircraft);
        this.setRegion(region);

        if (!(aircraft instanceof Gather)) {
            throw new AerialVehicleNotCompatibleException(
                    super.aircraft().getClass().getSimpleName(),
                    this.getClass().getSimpleName()
            );
        }
    }

    public String region() {
        return region;
    }

    private void setRegion(String region) {
        this.region = region;
    }

    protected String executeMission() {
        return String.format(
                "%s: %s Collecting Data in %s with: sensor type - %s",
                super.pilotName(),
                super.aircraft().getClass().getSimpleName(),
                this.region(),
                ((Gather) super.aircraft()).sensorType()
        );
    }
}
