package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.Capabilities.Bomber;
import Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(Coordinates destination,
                         String pilotName,
                         AerialVehicle aircraft,
                         String target) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, aircraft);
        this.setTarget(target);

        if (!(aircraft instanceof Bomber)) {
            throw new AerialVehicleNotCompatibleException(
                    super.aircraft().getClass().getSimpleName(),
                    this.getClass().getSimpleName()
            );
        }
    }

    public String target() {
        return target;
    }

    private void setTarget(String target) {
        this.target = target;
    }

    protected String executeMission() {
        return String.format(
                "%s: %s Attacking %s with: %sX%s",
                super.pilotName(),
                super.aircraft().getClass().getSimpleName(),
                this.target(),
                ((Bomber) super.aircraft()).missileType(),
                ((Bomber) super.aircraft()).missilesAmount()
        );
    }
}
