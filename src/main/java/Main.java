import AerialVehicles.AerialVehicle;
import AerialVehicles.Capabilities.Assessor;
import AerialVehicles.Capabilities.Bomber;
import AerialVehicles.Capabilities.Gather;
import AerialVehicles.Drones.Haron.Shoval;
import AerialVehicles.Drones.Hermes.Kochav;
import AerialVehicles.Planes.F16;
import AerialVehicles.Planes.F15;
import Entities.Coordinates;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import Missions.Mission;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Coordinates hatzerim = new Coordinates(31.1462, 34.4249);
        Coordinates palmachim = new Coordinates(31.5600, 34.4223);
        Coordinates target = new Coordinates(30.5486, 33.1828);

        Map<String, AerialVehicle> aircrafts = new HashMap<>();

        aircrafts.put("The Millennium Falcon", new F16(
                palmachim,
                6,
                Bomber.missileType.Spice250,
                Assessor.cameraType.Regular
        ));
        aircrafts.put("X-wing fighter", new F15(
                palmachim,
                4,
                Bomber.missileType.Amran,
                Gather.sensorType.Elint
        ));
        aircrafts.put("Jedi starfighter", new Shoval(
                hatzerim,
                2,
                Bomber.missileType.Amran,
                Gather.sensorType.Elint,
                Assessor.cameraType.Thermal
        ));
        aircrafts.put("ARC-170", new Kochav(
                hatzerim,
                2,
                Bomber.missileType.Python,
                Gather.sensorType.InfraRed,
                Assessor.cameraType.NightVision
        ));

        try {
            Mission[] missions = {
                    new IntelligenceMission(
                            target,
                            "R2-D2",
                            aircrafts.get("ARC-170"),
                            "The Death Star"
                    ),
                    new IntelligenceMission(
                            target,
                            "C-3PO",
                            aircrafts.get("Jedi starfighter"),
                            "The Death Star"
                    ),
                    new AttackMission(
                            target,
                            "Luke Skywalker",
                            aircrafts.get("X-wing fighter"),
                            "enemies aircraft and protecting The Millennium Falcon"
                    ),
                    new AttackMission(
                            target,
                            "Han Solo",
                            aircrafts.get("The Millennium Falcon"),
                            "The Death Star"
                    ),
                    new BdaMission(
                            target,
                            "Obi-Wan Kenobi",
                            aircrafts.get("ARC-170"),
                            "The Death Star"
                    )
            };

            missions[0].begin();
            missions[1].begin();

            missions[1].cancel();

            missions[0].finish();

            missions[2].begin();
            missions[3].begin();

            missions[2].finish();
            missions[3].finish();

            missions[4].begin();
            missions[4].finish();
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }
}
