package Missions;

public class AerialVehicleNotCompatibleException extends Exception {
    public AerialVehicleNotCompatibleException(String className, String mission) {
        super(String.format("%s Aircraft can not complete %s", className, mission));
    }
}
