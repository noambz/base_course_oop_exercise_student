package AerialVehicles.Drones.Haron;

import AerialVehicles.Capabilities.Bomber;
import AerialVehicles.Capabilities.Gather;
import Entities.Coordinates;

public class Eitan extends Haron implements Bomber, Gather {
    private int missilesAmount;
    private Bomber.missileType missileType;
    private Gather.sensorType sensorType;

    public Eitan(Coordinates homeBase,
                 int flightHours,
                 Status status,
                 int missilesAmount,
                 Bomber.missileType missileType,
                 Gather.sensorType sensorType) {
        super(homeBase, flightHours, status);
        this.setMissilesAmount(missilesAmount);
        this.setMissileType(missileType);
        this.setSensorType(sensorType);
    }

    public Eitan(Coordinates homeBase,
                 int missilesAmount,
                 Bomber.missileType missileType,
                 Gather.sensorType sensorType) {
        super(homeBase);
        this.setMissilesAmount(missilesAmount);
        this.setMissileType(missileType);
        this.setSensorType(sensorType);
    }

    public int missilesAmount() {
        return missilesAmount;
    }

    private void setMissilesAmount(int missilesAmount) {
        this.missilesAmount = missilesAmount;
    }

    public Bomber.missileType missileType() {
        return missileType;
    }

    private void setMissileType(Bomber.missileType missileType) {
        this.missileType = missileType;
    }

    public Gather.sensorType sensorType() {
        return sensorType;
    }

    private void setSensorType(Gather.sensorType sensorType) {
        this.sensorType = sensorType;
    }

}
