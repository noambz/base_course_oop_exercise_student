package AerialVehicles.Capabilities;

public interface Gather {
    public static enum sensorType {
        InfraRed,
        Elint
    }

    sensorType sensorType();
}
