package AerialVehicles.Planes;

import AerialVehicles.Capabilities.Bomber;
import AerialVehicles.Capabilities.Gather;
import Entities.Coordinates;

public class F15 extends Plane implements Bomber, Gather {
    private int missilesAmount;
    private missileType missileType;
    private sensorType sensorType;

    public F15(Coordinates homeBase,
               int flightHours,
               Status status,
               int missilesAmount,
               Bomber.missileType missileType,
               sensorType sensorType) {
        super(homeBase, flightHours, status);
        this.setMissilesAmount(missilesAmount);
        this.setMissileType(missileType);
        this.setSensorType(sensorType);
    }

    public F15(Coordinates homeBase,
               int missilesAmount,
               Bomber.missileType missileType,
               sensorType sensorType) {
        super(homeBase);
        this.setMissilesAmount(missilesAmount);
        this.setMissileType(missileType);
        this.setSensorType(sensorType);
    }

    public int missilesAmount() {
        return missilesAmount;
    }

    private void setMissilesAmount(int missilesAmount) {
        this.missilesAmount = missilesAmount;
    }

    public Bomber.missileType missileType() {
        return missileType;
    }

    private void setMissileType(Bomber.missileType missileType) {
        this.missileType = missileType;
    }

    public Gather.sensorType sensorType() {
        return sensorType;
    }

    private void setSensorType(Gather.sensorType sensorType) {
        this.sensorType = sensorType;
    }
}
