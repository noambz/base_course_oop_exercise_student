package AerialVehicles.Drones;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Drone extends AerialVehicle {
    public Drone(Coordinates homeBase, int flightHours, Status status) {
        super(homeBase, flightHours, status);
    }

    public Drone(Coordinates homeBase) {
        super(homeBase);
    }

    String hoverOverLocation(Coordinates destination) {
        if (this.status() != Status.ACTIVE) {
            System.out.println("Aerial Vehicle is not active");
            return null;
        } else {
            String ret = String.format("%s Hovering Over: %s, %s", this.getClass().getSimpleName(), destination.latitude(), destination.longitude());
            System.out.println(ret);
            return ret;
        }
    }
}
