package AerialVehicles.Capabilities;

public interface Bomber {
    public static enum missileType {
        Python,
        Amran,
        Spice250
    }

    int missilesAmount();
    missileType missileType();
}
