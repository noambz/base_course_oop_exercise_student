package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission {
    private Coordinates destination;
    private String pilotName;
    private AerialVehicle aircraft;

    public Mission(Coordinates destination,
                   String pilotName,
                   AerialVehicle aircraft) {
        this.setDestination(destination);
        this.setPilotName(pilotName);
        this.setAircraft(aircraft);
    }

    public Coordinates destination() {
        return destination;
    }

    private void setDestination(Coordinates destination) {
        this.destination = destination;
    }

    public String pilotName() {
        return pilotName;
    }

    private void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public AerialVehicle aircraft() {
        return aircraft;
    }

    private void setAircraft(AerialVehicle aircraft) {
        this.aircraft = aircraft;
    }

    public void begin() {
        System.out.printf("%s: Beginning Mission!\n", this.pilotName);
        System.out.printf("%s: ", this.pilotName);
        this.aircraft.flyTo(this.destination());
    }

    public void cancel() {
        System.out.printf("%s: Abort Mission!\n", this.pilotName);
        System.out.printf("%s: ", this.pilotName);
        this.aircraft.land(this.aircraft.homeBase());
    }

    public void finish() {
        System.out.printf("%s: Finish Mission!\n", this.pilotName);
        System.out.println(this.executeMission());
        System.out.printf("%s: ", this.pilotName);
        this.aircraft.land(this.aircraft.homeBase());
    }

    protected abstract String executeMission();
}
