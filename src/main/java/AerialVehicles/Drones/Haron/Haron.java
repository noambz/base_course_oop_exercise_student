package AerialVehicles.Drones.Haron;

import AerialVehicles.Drones.Drone;
import Entities.Coordinates;

public abstract class Haron extends Drone {
    private static final int maxFlightHourUntilRepair = 150;

    public Haron(Coordinates homeBase, int flightHours, Status status) {
        super(homeBase, flightHours, status);
    }

    public Haron(Coordinates homeBase) {
        super(homeBase);
    }

    public int maxFlightHourUntilRepair() {
        return maxFlightHourUntilRepair;
    }
}
