package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.Capabilities.Assessor;
import Entities.Coordinates;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(Coordinates destination,
                      String pilotName,
                      AerialVehicle aircraft,
                      String objective) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, aircraft);
        this.setObjective(objective);

        if (!(aircraft instanceof Assessor)) {
            throw new AerialVehicleNotCompatibleException(
                    super.aircraft().getClass().getSimpleName(),
                    this.getClass().getSimpleName()
            );
        }
    }

    public String objective() {
        return objective;
    }

    private void setObjective(String objective) {
        this.objective = objective;
    }

    protected String executeMission() {
        return String.format(
                "%s: %s Taking Pictures of %s with: %s camera",
                super.pilotName(),
                super.aircraft().getClass().getSimpleName(),
                this.objective(),
                ((Assessor) super.aircraft()).cameraType()
        );
    }
}
